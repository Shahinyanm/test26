<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('brand') }}'><i class='nav-icon la la-trademark'></i> {{ trans('menu_items.brand') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('carmodel') }}'><i class='nav-icon la la-bookmark'></i> {{ trans('menu_items.car_model') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('car') }}'><i class='nav-icon la la-car'></i> {{ trans('menu_items.cars') }}</a></li>
