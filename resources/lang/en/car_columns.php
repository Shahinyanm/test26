<?php

return [
    'car'              => 'Car',
    'manufacture_date' => 'Date of manufacture',
    'mileage'          => 'Mileage',
    'color'            => 'Color',
    'price'            => 'Price',
    'model'            => 'Model',
];
