<?php

return [
    'model'     => 'Model',
    'brand'     => 'Brand',
    'brands'    => 'Brands',
    'add_brand' => 'Brand',
];
