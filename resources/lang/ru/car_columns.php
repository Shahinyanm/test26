<?php

return [
    'car'              => 'Автомобиль',
    'manufacture_date' => 'Год выпуска',
    'mileage'          => 'Пробег',
    'color'            => 'Цвет',
    'price'            => 'Стоимость',
    'model'            => 'Модель',
];
