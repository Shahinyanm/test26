<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarRequest;
use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class CarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CarCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Car::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/car');
        CRUD::setEntityNameStrings(trans('car_columns.car'), trans('menu_items.cars'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('manufacture_date')
            ->type('date')
            ->format('Y')
            ->label(trans('car_columns.manufacture_date'));
        CRUD::column('mileage')->label(trans('car_columns.mileage'))->suffix(' km');
        CRUD::column('color')->label(trans('car_columns.color'));
        CRUD::column('price')->label(trans('car_columns.price'))->prefix('$');
        CRUD::column('model')->label(trans('car_columns.model'));
        CRUD::addColumn([
            'name'  => 'brand',
            'type'  => 'model_function',
            'label' => trans('brand_columns.brand'),
            'function_name' => 'getBrand'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */

        $this->crud->addFilter([
            'type'  => 'select2_multiple',
            'name'  => 'brand',
            'label' => trans('brand_columns.brand')
        ], function() {
            return Brand::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($values) { // if the filter is active (the GET parameter "draft" exits)
            $values = json_decode($values);
            $carIds = Car::whereHas('model', function($query) use ($values) {
                $query->whereIn('brand_id', $values);
            })->pluck('id')->toArray();
            $this->crud->addClause('whereIn', 'id', $carIds);
        });
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CarRequest::class);

        CRUD::addField([
            'name' => 'manufacture_date',
            'type' => 'date_picker',
            'label' => trans('car_columns.manufacture_date'),
            'date_picker_options' => [
                'format'      => "yyyy",
                'viewMode'    => "years",
                'minViewMode' => "years"
            ]
        ]);
        CRUD::field('mileage')->label(trans('car_columns.mileage'));
        CRUD::field('color')->label(trans('car_columns.color'));
        CRUD::field('price')->label(trans('car_columns.price'));
        CRUD::addField([
            'name'  => 'brand',
            'type' => 'select2_from_array',
            'options' => Brand::all()->pluck('name', 'id')->toArray(),
            'label' => trans('brand_columns.brand'),
        ]);
        CRUD::addField([
            'name' => 'model_id',
            'type' => 'relationship',
            'ajax' => true,
            'dependencies' => ['brand'],
            'minimum_input_length' => 1,
            'attribute' => 'name',
            'label' => trans('car_columns.model'),
            'include_all_form_fields' => true
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Show operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-show
     * @return void
     */
    protected function setupShowOperation()
    {
        CRUD::column('manufacture_date')
            ->type('date')
            ->format('Y')
            ->label(trans('car_columns.manufacture_date'));
        CRUD::column('mileage')->label(trans('car_columns.mileage'));
        CRUD::column('color')->label(trans('car_columns.color'));
        CRUD::column('price')->label(trans('car_columns.price'));
        CRUD::column('model')
            ->type('relationship')
            ->attribute('name')
            ->label(trans('car_columns.model'));
        CRUD::addColumn([
            'name'  => 'brand',
            'type'  => 'model_function',
            'label' => trans('brand_columns.brand'),
            'function_name' => 'getBrand'
        ]);
    }

    public function fetchModel(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');
        $options = CarModel::query();

        if (! $form['brand']) {
            return [];
        }

        if ($form['brand']) {
            $options = $options->where('brand_id', $form['brand']);
        }

        if ($search_term) {
            $options = $options->where('name', 'LIKE', '%' . $search_term . '%');
        }


        return $options->paginate(10);
    }
}
