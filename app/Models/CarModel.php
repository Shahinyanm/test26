<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
    use HasFactory, SoftDeletes, CrudTrait;

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($carModel) {
            $carModel->cars()->delete();
        });
    }

    protected $fillable = [
        'name',
        'brand_id',
    ];

    protected $hidden = [
        'brand_id',
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'model_id', 'id');
    }
}
