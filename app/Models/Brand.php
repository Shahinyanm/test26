<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory, CrudTrait;

    protected $fillable = ['name'];

    protected static function boot() {
        parent::boot();
        static::deleted(function(Brand $brand) {
            $modelIds = $brand->models->pluck('id');
            Car::whereIn('model_id', $modelIds)->delete();
            CarModel::whereIn('id', $modelIds)->delete();
      });
    }

    public function models()
    {
        return $this->hasMany(CarModel::class);
    }
}
