<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use HasFactory, SoftDeletes, CrudTrait;

    protected $fillable = [
        'manufacture_date',
        'mileage',
        'color',
        'price',
        'model_id',
    ];

    protected $hidden = [
        'model_id',
    ];

    public function model()
    {
        return $this->belongsTo(CarModel::class, 'model_id', 'id');
    }

    public function getBrand()
    {
        return $this->model->brand->name;
    }
}
